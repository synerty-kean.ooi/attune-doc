.. image:: https://readthedocs.org/projects/synerty-attune/badge/?version=latest
    :target: http://synerty-attune.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

==============
Synerty Attune
==============


QUALITY ASSURANCE
-----------------

Attune’s automated procedures slingshot quality assurance.
Run, test and rerun procedures in development environments.
Then run the fully tested procedure in production.

AUTOMATED AND FAST
------------------

Cut costs, Automate small and large server administration tasks.
Configure an Attune procedure once and reap the cost benefits of fast, unattended automation.

SECURE SELF HELP DASHBOARD
--------------------------

Spare your IT Admins. Attune’s self help dashboard allows normal users to help them selves.
The admins configure frequently requested procedures, and give normal users limited ability to just click run.

PORTABLE, SHARABLE PROCEDURES
-----------------------------

Attune’s procedures can be exported to a file, and imported back into Attune.
These procedure files contain all the scripts, comments, and even archives or installers,
ready to drop into another Attune server and execute.

CENTRALISE TEAM KNOWLEDGE
-------------------------

Attune provides a central source of truth for procedures, scripts, files, comments.
Attune’s self documenting configuration gives your whole team visibility and
capability to run and debug even the most specialised procedures.

POWERFUL MULTI SERVER COORDINATION
----------------------------------

Attune can run steps on multiple servers as multiple users within the one procedure,
allowing unequalled environment wide coordination.
