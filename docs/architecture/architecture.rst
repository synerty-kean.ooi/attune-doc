.. _architecture:

###########################
Architecture and Deployment
###########################

This page provides a high level overview of Attune's software architecture.

Attune is available as a desktop application or deployed as a Server for the
web application.

Attune Desktop Application
==========================

The Attune App that can be downloaded, installed and run in minutes.
The Attune App only runs jobs while the app is open. The Attune
application is supported on the follow operating systems:

* MacOS
* Windows

App Resource Requirements:

**Minimum:** 1 CPU, 512MB RAM, 500MB

**Recommended:** 2 CPUs, 2GB RAM, 1GB


Attune Server and Web Application
=================================

The Attune Server is a headless, standalone server running in a cloud or
server environment, using a Web App as the admin interface. The
following web browsers are supported:

* Brave
* Chrome
* Mozilla Firefox
* Microsoft Edge

Attune Server is deployed as a VM Template and is supported on the
following operating systems:

* RHEL 7
* RHEL 8
* CentOS 7
* CentOS 8
* Oracle Linux 7
* Oracle Linux 8

The following diagram shows two instances of Attune server deployed
in a Pre-Production and Production environments.

.. image:: https://www.servertribe.com/wp-content/uploads/2022/05/After_Attune-2.jpg

App Resource Requirements:

**Minimum:** 1 CPU, 512MB RAM, 6GB

**Recommended:** 8 CPUs, 8GB RAM, 500GB

**Learn more, 
`book an online demonstration! <https://www.servertribe.com/request-a-demo/>`_**
