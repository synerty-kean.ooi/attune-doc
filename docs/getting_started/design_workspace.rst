.. _getting_started_automation_design:

################
Design Workspace
################

Your Automated and Orchestrated IT processes are developed and modified in
individual Projects. The Design Workspace contains your Projects.

Projects
========

Projects are self contained and sharable. Your Projects contain your:

* Parameters,
* Files,
* Steps &
* Blueprints.

You can have many Projects and Projects can
contain many Parameters, Files, Steps and Blueprints.

Projects are version controlled with GIT integration. You can clone, push
and pull Projects with repositories such as
`GitHub <https://github.com/servertribe>`_

Parameters
==========

Parameters are used in Blueprints and substituted with Values when
a job is run. Values are mapped against Parameters in the Plan.
This provides maximum Blueprint re-usabiliity.

All Parameters have the same basic attributes regardless of
their type.

A project as a minimum requires a Node type and Credential type
Parameters to create a Step in a Blueprint. Parameters can be
created in the Step.

To learn how to reference Parameters in a Script read:
:ref:`Referencing Parameters in Scripts <getting_started_automation_referencing_parameters>`

Files
=====

Your Files can be stored centrally in Attune. Files are divided into
two categories:

* Version Controlled,
* Large Files.

**Version Controlled Files**

Version Controlled Files are maintained in the GIT repository. These are
typically small text files.

Dynamically create your **Configuration Files** when your Job is run.
Attune has a template engine that allows you to deploy files which contain
markup using Mako, an embedded Python language. The template engine processes
the files in a configuration archive and renders the final outputs.

More information about Mako templates, and the syntax used for the
configuration archives files can be found at http://www.makotemplates.org

**Large Files**

Large Files are not stored in the GIT repository. These can be large
zipped directories, installation file, ISO's, etc.

Steps & Blueprints
==================

Blueprints orchestrate the sequence and paralleling of Steps. Steps
automate individual scripted actions. Blueprints from other Projects
can be linked into a Blueprint.
