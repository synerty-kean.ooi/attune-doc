.. _getting_started_automation:

###########################
Getting Started with Attune
###########################

Start using ServerTribe's Attune RIGHT NOW, it's quick and easy to get started.

If you don't have Attune, 
`Download Attune App <https://www.servertribe.com/comunity-edition/>`_.

If you want the Attune Server platform, please 
`contact ServerTribe <https://www.servertribe.com/contact-us/>`_.


.. raw:: html

    <embed>
        <iframe height="500pt" width="700pt"
        src="https://www.youtube-nocookie.com/embed/n3rh_cl2mUk"
        title="YouTube video player" frameborder="0" allow="accelerometer;
        autoplay; clipboard-write; encrypted-media; gyroscope;
        picture-in-picture" allowfullscreen></iframe>
    </embed>

:Video: Intro to Attune in 2 Minutes

Attune’s IT Automation & Orchestration is divided into 3 spaces:

* Design,
* Plan &
* Run.

**Design**

The :ref:`Design Workspace <getting_started_automation_design>` is where
you create your automated and reusable building blocks.

**Plan**

The :ref:`Plan Workspace <getting_started_automation_plan>` is where
you’ll plan a job to run a blueprint and which values are connected to the
parameters in the blueprint. A single blueprint can be used for many jobs
with different values.

Schedules can be planned to orchestrate multiple Jobs.

**Run**

The :ref:`Run Workspace <getting_started_automation_run>` is where you
control your Jobs, Debug your jobs and view
your historical logs.


.. toctree::
    :caption: Getting Started with Automation Contents
    :titlesonly:
    :hidden:
    :maxdepth: 2
    :name: gettingstartedautomationtoc

    design_workspace
    plan_workspace
    run_workspace
