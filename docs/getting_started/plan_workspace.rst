.. _getting_started_automation_plan:

##############
Plan Workspace
##############

Your Jobs are planned to run Blueprints with substituted Values
controlled via the Self-Service Portal, Jobs Workspace or
Scheduler. The Jobs are created in the Plan Workspace.

The **Plan Workspace** contains your:

* Values,
* Plans &
* Schedules.

Values
======

Values are substituted into Parameters when a Jobs is run.
Values have different attributes based on their type.

To learn the Values type attributes read:
:ref:`Referencing Parameters in Scripts <getting_started_automation_referencing_parameters>`

Plans
=====

Plans allow you to assign Values to the Parameters that appear in
a Blueprint. You can create many Plans for a single Blueprints
with different Values assigned.

Schedules
=========

Schedules can be configured to run many Plans either sequentially or
in parallel. The Attune service must be running for the Scheduler to run.
