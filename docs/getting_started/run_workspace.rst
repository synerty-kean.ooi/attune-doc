.. _getting_started_automation_run:

#############
Run Workspace
#############

Control your Jobs, debug your Steps and review your Historical Logs
in the Run Workspace.

The **Run Workspace** contains your:

* Jobs &
* Historical Logs.

Jobs
====

Jobs allow you to start, pause or abort individual Jobs. Individual
Steps can be inspected and modified. Jobs can be run from any Step
in the procedure allowing you to skip sections or repeat a Step.

If a Step is modified it will be updated in the Blueprint.
If the Blueprint structure of Steps is modified the Job will
need to be recreated to populate the new, deleted or reorganised Steps.

Recreating the Job will archive the current log for each Step.

Historical Logs
===============

Historical Logs are captured after each Scheduled Job,
Self-Server Portal run job and recreated Job.

The Historical Logs can be viewed in Attune or exported in either PDF or
HTML formats.
