.. howto_:

######
How To
######

.. toctree::
    :maxdepth: 1

    design_workspace/clone_project
    plan_workspace/create_job
    setup_winrm/setup_winrm_cifs_manually
    setup_winrm/setup_winrm_via_ad
    scripts_referencing_parameters/scripts_referencing_parameters
