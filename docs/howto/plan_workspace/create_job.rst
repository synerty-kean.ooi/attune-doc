.. _create_job:

============
Create a Job
============

Objective
---------

This procedure provides instructions to create adn run a Job in Attune.

Procedure
---------

Create a Job
````````````

Navigate to the Plan workspace and create a Job from a Blueprint in the
Project you cloned.

#.  Open the Plan Workspace

#.  Navigate to Jobs

#.  Enter your Job Name

#.  Select the Blueprint to use

#.  Select Create

.. image:: create_job_01.png

----

Create the Values required to fill the Parameters for the Job.

#.  Navigate to Values

#.  Select the Value Type

#.  Enter the Value Name

#.  Select Create

.. image:: create_job_02.png

.. note:: After selecting the Type of Value you'll be provided fields to
    complete.

----

Configure the Parameters for the Job you created.

#.  Select the Job

#.  Navigate to Inputs

#.  Populate the Values

#.  Select Save

.. image:: create_job_03.png

Run a Job
`````````

Run your Job.

#.  Open the Run Workspace

#.  Select the Job

#.  Select Run

.. image:: run_job_01.png

Complete
--------

This procedure is now complete, you've successfully run a Job in Attune.
