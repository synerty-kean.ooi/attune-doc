.. _getting_started_automation_referencing_parameters:

#################################
Referencing Parameters in Scripts
#################################

**Blueprints** are composed of **Steps**, and each **Step** has a unique name
and a number of other attributes, including a **Script**, where the actual
commands for executing the **Step** are written. In these commands, curly
braces { } are used to delimit **Values** which are referenced by
**Parameters**.

In order to be used in a **Script** the **Parameters names are stripped of any
separating spaces between words and converted to camelcase - with only the
first letter of the second, and any subsequent words, capitalised.

So if a **Parameter** is named - **THIS IS my name** it would be converted to
:code:`thisIsMyName`

**Values** are referenced in scripts as
:code:`{placeHolderName.placeValueAttribute}`

So for a **Parameter** with the name `THIS IS my name` of type **Server**,
we can refer to the IP address in a script as
:code:`{thisIsMyName.ip}`

The expressions contained in the curly braces are evaluated prior to **Job**
being **Run**, and the curly brace expressions are replaced by actual **Values**
. In the example above
:code:`{thisIsMyName.ip}`
would be substituted with the IP attribute of the **Value** that was assigned
to the **Parameter** - `THIS IS my name` in the Plan.

This substitution of **Values** is performed by Attune, so using the curly
Brace {} notation to reference attribute **Values** works with all **Script**
regardless of the type of **Script**.

Value Attributes
================

This section details the **Value** attributes for each **Parameter** type.

OS Credential
-------------

Attributes of `OS Credential` **Parameter** type can be referenced as follows:

===============  ==============  =====================================
Value Attribute  Attribute Name  Script Reference
===============  ==============  =====================================
Name             name            :code:`{myPlaceHolder.name}`
User             user            :code:`{myPlaceHolder.user}`
Password         password        :code:`{myPlaceHolder.password}`
Comment          comment         :code:`{myPlaceHolder.comment}`
===============  ==============  =====================================

SQL Credential
--------------

Attributes of `SQL credential` **Parameter** type can be referenced as follows:

===============  ==============  =====================================
Value Attribute  Attribute Name  Script Reference
===============  ==============  =====================================
Name             name            :code:`{myPlaceHolder.name}`
User             user            :code:`{myPlaceHolder.user}`
SID              sid             :code:`{myPlaceHolder.sid}`
Password         password        :code:`{myPlaceHolder.password}`
As Sysdba        asSysDba        :code:`{myPlaceHolder.asSysDba}`
Comment          comment         :code:`{myPlaceHolder.comment}`
===============  ==============  =====================================

Node
----

Attributes of `Node` **Parameter** type are referenced as follows:

===============  ==============  =====================================
Value Attribute  Attribute Name  Script Reference
===============  ==============  =====================================
Name             name            :code:`{myPlaceHolder.name}`
IP Address       ip              :code:`{myPlaceHolder.ip}`
Hostname         hostname        :code:`{myPlaceHolder.hostname}`
Domain Name      domain          :code:`{myPlaceHolder.domain}`
Comment          comment         :code:`{myPlaceHolder.comment}`
===============  ==============  =====================================

Node List
---------

Node List is a comma separated list of node **Parameters** (CSV).

Attributes of `Node List` **Parameter** type are referenced as follows:

===============  ===============  =====================================
Value Attribute  Attribute Name   Script Reference
===============  ===============  =====================================
Name             name             :code:`{myPlaceHolder.name}`
Server           serverNames      :code:`{myPlaceHolder.serverNames}` as CSV
Server           serverIps        :code:`{myPlaceHolder.serverIps}` as CSV
Server           serverHostnames  :code:`{myPlaceHolder.serverHostnames}` as CSV
Server           serverTypes      :code:`{myPlaceHolder.serverTypes}` as CSV

                                  The types are:
                                        *   1 = Linux
                                        *   2 = Windows

Server           serverDomains    :code:`{myPlaceHolder.serverDomains}` as CSV
Server           serverFqns       :code:`{myPlaceHolder.serverFqns}` as CSV
Comment          comment          :code:`{myPlaceHolder.comment}`
===============  ===============  =====================================

IPv4 Subnet
-----------

Attributes of `IPv4 Subnet` **Parameter** type are referenced as follows:

===============  ==============  =====================================
Value Attribute  Attribute Name  Script Reference
===============  ==============  =====================================
Name             name            :code:`{myPlaceHolder.name}`
Subnet           subnet          :code:`{myPlaceHolder.subnet}`
Netmask          netmask         :code:`{myPlaceHolder.netmask}`
Gateway IP       gateway         :code:`{myPlaceHolder.gateway}`
DNS IP 1         dns1            :code:`{myPlaceHolder.dns1}`
DNS IP 2         dns2            :code:`{myPlaceHolder.dns2}`
Comment          comment         :code:`{myPlaceHolder.comment}`
===============  ==============  =====================================


Text
----

Attributes of `Text` **Parameter** type are referenced as follows:

===============  ==============  =====================================
Value Attribute  Attribute Name  Script Reference
===============  ==============  =====================================
Name             name            :code:`{myPlaceHolder.name}`
Value            value           :code:`{myPlaceHolder}`

                                  Please note the text value is referenced
                                  without the attribute.

Comment          comment         :code:`{myPlaceHolder.comment}`
===============  ==============  =====================================
