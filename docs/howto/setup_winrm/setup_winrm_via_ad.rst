.. _setup_winrm_via_ad:

==================
Setup WinRM Via AD
==================

Setting up WinRM via Active Directory:

Objective
---------

This procedure provides instructions to automatically enable WinRM with HTTPS via Active Directory group policies.

Attune uses WinRM to execute commands on windows desktops and servers. WinRM, combined with improvements in PowerShell Cmdlets is Microsofts emerging solution for scriptable administration of windows servers.

.. note:: If you don't have a domain and the target computers joined to the domain, then this procedure isn't for you.

.. note:: This setup is straight forward with defaults, your corporate environment may require alterations to the procedure.

Setup :

#.  Windows 2012 R2 Server, with Active Directory Domain Services configured.

#.  Target servers are joined to the domain.

Procedure
---------

The following procedure all performed via a Remote Desktop session to the domain server.

Adding Certificate Server Role
``````````````````````````````

#.  Open the Server Manager

#.  Select "Add roles and features"

.. image:: add_certificate_role.png

----

Click through the "Before You Begin" screen

----

On the "Installation Type screen" :

#.  Select "Role-based or feature-based installation

#.  Click "Next"
 
.. image:: add_certificate_role_based.png


----

On the "Server Selection" screen :

#.  Select the server to install the Certificate service on

#.  Click "Next"

.. image:: add_role_server_selection.png

----

On the "Server Roles" screen:

#.  Select "Active Directory Certificate Service"

#.  On the popup, click "Add Features"

#.  Click Next.

.. image:: add_role_server_roles_1.png

.. image:: add_role_server_roles_2.png

----

On the "Features" screen:

#.  Click "Next"
 

----

On the "AD CS" screen:

#.  Click "Next"

----

On the "Role Services" screen:

#.  Click "Next"

.. image:: add_role_role_services.png

----

On the "Confirmation" screen:

#.  Check the "Restart the destination server automatically if required"

#.  Click "Yes" on the confirmation dialog.

#.  Click "Install"


.. image:: add_role_confirm_1.png

.. image:: add_role_confirm_2.png

----

The installation will proceed, break time.

----

On the "Results" screen:

#.  Click "Close"

----

Repeat the procedure for the other domain controllers in the domain.

Configuring Certificate Server Role
```````````````````````````````````

#.  Open the Server Manager

#.  Select "Notification" dropdown

#.  Click "Configure Active Directory Certificate …"

.. image:: ad_config_start.png

----

On the "Credentials" screen:

#.  Ensure you have entered a valid domain credential

#.  Click "Next"

.. image:: ad_config_credentials.png


----

On the "Role Services" screen:

#.  Check "Certificate Authority"

#.  Click "Next"

.. image:: ad_config_role_services.png

----

On the "Setup Type" screen:

#.  Ensure "Enterprise CA" is selected

#.  Click "Next"

.. image:: ad_config_setup_type.png

----

On the "CA Type" screen:

#.  Ensure "Root CA" is selected, or "Subordinate CA" if this is the second server your setting up.

#.  Click "Next"

.. image:: ad_config_ca_type.png



----

On the "Private Key" screen:

#.  Ensure "Create a new private key" is selected

#.  Click "Next"

.. image:: ad_config_private_key.png


----

If this IS the first server your setting up and the Root CA, skip this step.

If this is the second server you're setting up, it will be a Subordinate CA and need to request signing from the root CA.

#.  Select the "Certificate Request" on the screen list on the side bar

#.  Click "Send a certificate request to a parent CA"

#.  Click "Select"

#.  Click "Next"

.. image:: ad_config_certificate_request.png

----

#.  Click "Confirmation" on the screen list on the side bar

#.  Then click "Configure

.. image:: ad_config_confirmation.png


----

Repeat the procedure for the other domain controllers in the domain.

When prompted at the "CA Type", you will need to select "Subordinate CA" on the subsequent servers.

Configure WinRM Certificate Template
````````````````````````````````````

.. note:: Be sure to check the Certificate Services setting updates in the following section on all domain controllers.

Open "Certificate Authority" (Use the start menu search)

#.  Expand the root server

#.  Select "Certificate Templates"

#.  Right click and select "Manage"

.. image:: new_cert_certsrv.png

----

In the "Certificate Template Console" app

#.  Find the "Web Server" template in the list

#.  Right click and select "Duplicate Template"

.. image:: new_cert_webserver.png

----

In the "Properties of New Template" app


#.  Select the "General" tab

#.  Enter "WinRM in the "Template display name"

.. image:: new_cert_properties.png


----


#.  Select the "Subject Name" tab

#.  Select "Build from this Active Directory information"

#.  Select "Common name" for the "Subject name format"

#.  Check "User principle name (UPN)"

.. image:: new_cert_subject_name.png

----


#.  Select the "Security" tab

#.  Select "Add"

    .. image:: new_cert_security_1.png

    #.  On the "Select Users, Computers…" screen:

    #.  Select "Object Types

        #.  On the "Object Types" screen

        #.  select "Computers"

        #.  click "Ok"

        .. image:: new_cert_security_2.png

    #.  Back on the "Select Users, …" screen, Enter "Domain Computers" in the "Enter the object names to select" box

    #.  Click "Check Names"

    #.  Click "Ok"

    .. image:: new_cert_security_3.png

#.  Back on the "Properties" screen, Select "Domain Computers"

#.  Select "Enrol"

#.  Select "Autoenroll"

#.  Click "Ok"

.. image:: new_cert_security_4.png



----

Back in the "Certificate Authority" app

#.  Right click on "Certificate Templates"

#.  Select "New"

#.  Select "Certificate Template to Issue"

.. image:: add_cert_new_issue.png


----

On the "Enable Certificate Templates" popup:

#.  Find and select the created "WinRM" certificate template.

#.  Click "Ok"

.. image:: new_cert_enable_cert_template.png

Create the Group Policy Object
``````````````````````````````

The group policy object will automatically enable WinRM on Windows operating systems joined to the domain.

----

Open the "Group Policy Management" app


#.  Expand the Forest

#.  Expand the Domains

#.  Expand the Domain

#.  Select the "Group Policy Objects"

#.  Right click and select "New"

.. image:: new_gpo_new.png

----

On the "New GPO" dialog


#.  Enter "Configure WinRM" in the "Name" field

#.  Click "OK"

.. image:: new_gpo_name.png


----

In the "Group Policy Objects" list:


#.  Right click on "Configure WinRM"

#.  Select "Edit"

.. image:: new_gpo_edit.png

----

Enabling Autoenroll of Certificate Services
```````````````````````````````````````````

In the "Configure WinRM" Group Policy:


#.  Expand "Computer Configuration" → "Policies"

#.  Expand "Windows Settings"

#.  Expand "Security Settings"

#.  Select "Public Key Policies"

#.  On the right hand pane, double click "Certificate Services Client – Auto-Enrollement"

.. image:: edit_gpo_enroll_editor.png

----

In the "Certificate Services Client – Auto-Enroll…" properties:


#.  Set "Configuration Model" to "Enabled"

#.  Check "Renew expired certificates…"

#.  Check "Update certificates that user certificate templates"

#.  Click "OK"

.. image:: edit_gpo_enroll_properties.png

----

Configure Enrolment Script
``````````````````````````

Expand the following


#.  Expand "Computer Configuration" → "Policies"

#.  Expand "Windows Settings"

#.  Expand "Scripts"

#.  Double click on "Startup"

.. image:: edit_gep_enroll_script.png

----

On the "Startup Properties"

#.  Click "Show Files"

.. image:: edit_gpo_enroll_script_properties.png

----

In windows explorer


#.  Click "View"

#.  Ensure "File name extensions" is checked

#.  Right click on a blank space in the window

#.  Select "New"

#.  Select "Text Document"

.. image:: edit_gpo_enroll_script_explorer.png

----

In windows explorer


#.  Rename the file to "enable_winrm_https.bat"

#.  Enter the following as the file contents ::


        winrm quickconfig -q -transport:https


#.  Save the file and close notepad

#.  Close windows explorer

.. image:: edit_gpo_enroll_script_notepad.png

----

Back at the "Startup Properties" screen


#.  Click "Add"

.. image:: edit_gpo_enroll_script_startup_1.png

#.  On the Add Script Diaglog

    #.  Click "Browse"

    #.  Select the "enable_winrm_https.bat"

    #.  Click Ok

    .. image:: edit_gpo_enroll_script_startup_2.png

#.  Click "Ok" on the Startup Properties dialog.




Configure Firewall for WinRM
````````````````````````````

Expand the following


#.  Expand "Computer Configuration"

#.  Expand "Policies"

#.  Expand "Windows Settings"

#.  Expand "Security Settings"

#.  Expand "Windows Firewall with Advanced Security"

#.  Expand "Windows Firewall with Advanced Security – …."

#.  Right click on "Inbound Rules"

#.  Click "New Rule"

.. image:: edit_gpo_firewall_new.png

----

On the "New Inbound rule Wizard"


#.  Click "Predefined"

#.  Select "Windows Remote Management"

#.  Click "Next"

.. image:: edit_gpo_firewall_inbound_rule.png

----

On the "Predefined Rules" screen


#.  Click "Next"

.. image:: edit_gpo_firewall_predefined_rule.png

----

On the "Action" screen


#.  Click "Finish"

.. image:: edit_gpo_firewall_action.png


Enable WinRM
````````````

Expand the following


#.  Expand "Computer Configuration"

#.  Expand "Preferences"

#.  Expand "Control Panel Settings"

#.  Expand "Services"

#.  Right click on "Services"

#.  Select "New

#.  Select "Service"

.. image:: edit_gpo_enable_winrm_service_new.png


----

On the "Predefined Rules" screen


#.  Change "Startup" to "Automatic (Delayed Start)"

    .. image:: edit_gpo_enable_winrm_service_new_name.png

#.  Change "Service name:" to "WinRM"

    .. image:: edit_gpo_enable_winrm_service_properties.png

#.  Click "OK"



Tweak WinRS
```````````

Expand the following


#.  Expand "Computer Configuration"

#.  Expand "Policies"

#.  Expand "Administrative Template Policy"

#.  Expand "Windows Components"

#.  Expand "Windows Remote Shell


.. image:: edit_gpo_tweak_rs_tree.png

----

In the Settings pane:


#.  Enable and Set **"Specify maximum amount of memory in MB per shell"**, to 1024

#.  Enable and Set **"Specify maximum number of processes per shell"**, to 64

#.  Enable and Set **"Specify maximum number of remote shells per user"**, to 64

.. image:: edit_gpo_tweak_rs_options.png


Linking Group Policy
````````````````````

The group policy is now complete. Link the group policy to the desired OUs, and reboot the target servers.

Complete
--------

This procedure is now complete, You can Create new Windows Server values in Attune and set the WinRM specification to "WinRM 2.0 HTTPS"
