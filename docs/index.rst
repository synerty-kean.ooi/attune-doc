.. _attune_documentation:

####################
Attune Documentation
####################

Tedious repetitive tasks are bogging you down and wasting your precious 
time. Tasks are missing deadlines and you are not as efficient as you 
know you can be.

Our software solution Attune automates and orchestrates processes to 
streamline deployments, scaling, migrations, and management of your 
systems. We’re using the Attune platform to build a community of sharable 
automated and orchestrated processes.

System Administrator’s can leverage the publicly available orchestrated 
blueprints to increase their productivity, and accelerate the delivery of 
projects. System Administrator’s can open-source their own and improve 
existing community orchestrated projects.

The collective power of a community of talented individuals working in 
concert delivers not only more ideas, but quicker development and 
troubleshooting when issues arise.

What is Attune
==============

Attune **automates and orchestrates scripts, commands, and processes**
exactly as they would be manually typed into a device.

Easily and quickly develop simple and bespoke IT/OT automation with
ServerTribe's Attune.

* Build with Popular Scripting Languages
* Rapidly Build Automated Processes
* Portable and Sharable IP
* Full-stack Multi-server Orchestration
* Centralised Scheduler
* Automated Document Generation

Attune is our **flexible IT Automation & Orchestration** solution, a **self-documenting**
central source of **reusable proven processes**, files and backups to build and
maintain your IT/OT infrastructure. Attune is used on enterprise critical infrastructure
through to NUCs and home network infrastructure.

.. raw:: html

    <embed>
        <iframe height="500pt" width="700pt"
        src="https://www.youtube-nocookie.com/embed/xzs0xGPar78"
        title="YouTube video player" frameborder="0" allow="accelerometer;
        autoplay; clipboard-write; encrypted-media; gyroscope;
        picture-in-picture" allowfullscreen></iframe>
    </embed>

:Video: Demonstration of creating an automated process in Attune.

If you'd like a live online demonstration of Attune and the opportunity to ask questions,
please `contact ServerTribe <https://www.servertribe.com/request-a-demo/>`_.

Get Attune!
===========

`Download Attune App <https://www.servertribe.com/comunity-edition/>`_

The Attune desktop app is available for download through the
`ServerTribe website <https://www.servertribe.com/>`_
and the Community Edition is free to use.

Visit the ServerTribe website to review
`Attune's subscription licensing <https://www.servertribe.com/solutions-services/products/attune-licensing/>`_
models.

If you'd like the Attune server, please
`contact ServerTribe <https://www.servertribe.com/contact-us/>`_.

Engage the ServerTribe Community
--------------------------------

* `ServerTribe Discord <https://discord.com/invite/3YpJsagqUn>`_
* `ServerTribe YouTube <https://www.youtube.com/channel/UCLRvZajNQXfQPJnYFdeXZ3w>`_
* `ServerTribe Github <https://github.com/servertribe>`_

About Attune
============

**Value Proposition:**

* Reusable IT automated & orchestrated processes.
* Self-service Portal for the business to run jobs.
* Self-documenting central source of truth.

**Use Cases:**

* Provision Environments & Servers
* OS Upgrades & Configuration Changes
* Hardware Replacements
* Application Deployment
* Oracle Database Upgrades
* Dataset Backups & Restoration
* Disaster Recovery

**Business Value:**

* Accelerate Project Delivery
* Reduce Overheads
* Improve Key Resource Productivity
* Proven Process for Consistency
* Eliminate Human Error
* Knowledge Capture

**Learn more,**
`book an online demonstration! <https://www.servertribe.com/request-a-demo/>`_


.. toctree::
    :caption: Documentation Contents
    :includehidden:
    :numbered:
    :maxdepth: 2
    :name: mastertoc

    it_automation_orchestration/it_automation_orchestration
    getting_started/getting_started_automation
    architecture/architecture
    howto/howto
    glossary/glossary
    release_notes/IndexReleaseNotes
