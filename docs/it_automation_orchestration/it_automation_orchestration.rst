.. _it_automation_orchestration:

##################################
IT/OT Automation and Orchestration
##################################

Attune **automates and orchestrates scripts, commands, and processes**
exactly as they would be manually typed into a device.

Attune is an **agentless solution** that connects to nodes and operating
systems with **WinRM and ssh protocols**. Attunes steps are written in the
popular shell scripting languages:

* Batch
* PowerShell
* Bash
* Python
* Perl
* SQL

Rapidly build your first Attune automated jobs copying your existing scripts to
begin your automation journey.

If you don't have Attune, 
`Download Attune App <https://www.servertribe.com/comunity-edition/>`_.

If you want the Attune Server platform, please 
`contact ServerTribe <https://www.servertribe.com/contact-us/>`_.

**Rapidly Debug Your Scripts and Continue The Job**

Attune provides the ability to modify scripts in the steps during a running job
without requiring the job to be restarted.

Traditionally an error in a procedure could stop the entire process, requiring the
process to be restarted. By writing steps that remain unchanged when run multiple
times, these steps can be tested and re-tested without refreshing the system and
the job can be continued.

Rapidly deliver automated procedures with Attune’s simple in job debugging and
editing.

**Portable, Sharable Procedures**

Your team can export Attune’s procedures to a file, and then import them into
another Attune. These procedure exports contain all the scripts, comments, and
even archives or installers, ready to drop into another Attune server and execute.

Attune has variables, used to create reusable procedures.

Variables take the place of servers, strings, passwords, usernames, and more.

Reusable procedures reduces time, effort, and improve quality assurance.

**Full-stack Orchestration**

Build physical servers from bare metal with DELL’s iDRAC. Attune uses the redfish
APIs to shutdown, boot, reboot, and install the operating system on physical
servers.

Spin up and rebuild virtual machines, increase disk space, add network adapters,
and install operating systems.

* iDRAC
* ESXi
* VMWare Workstation
* oVirt
* RHEV
* KVM
* Parallels
* VirtualBox

**Powerfully Simple Multi-Server Coordination**

Attune can run steps on multiple servers, as multiple users, within the one Job,
enabling environment wide coordination.

**Simplify Scheduled Tasks**

Simplify your teams management of scheduled tasks across the data centre. Attune
has a built in job scheduler that centralises the management of scheduled tasks
across Windows and Linux servers.

**Automated Document Generation**

Attune inherently captures and centralises your support team’s knowledge.

Captures logs of jobs in a centralised location. Historical logs are archived for
future investigation.

Industry ITIL requirements have driven Attunes unique design to generate and
export the step by step documentation.

Your team can export the automated procedures as instructions to run the steps
manually. This is helpful for working in an isolated environment without Attune
access.

**Learn more, 
`book an online demonstration! <https://www.servertribe.com/request-a-demo/>`_**
