.. _release_notes_v1.3.x:


=============================
Release notes - Attune v1.3.x
=============================

v1.3.4
------

**Improvement**

AT-119 Add support for referencing Server List from text

**Bug**

AT-118 Attune can't use place value attributes that are ints

AT-120 sort drop down list for place values in plan screen

v1.3.3
------

**Bug**

AT-114 sudo: unable to resolve host causes ServerID gen failure

AT-115 License spelling mistake in license entry box label

AT-116 RHEL5 requires full path to dmidecode for license

AT-117 License fails, "sudo: sorry, you must have a tty to run sudo"


v1.3.2
------

**New Feature**

AT-113 ServerID generation repeats last 4 chars twice.

**Bug**

AT-111 attune ignores sudo dmidecode if it fails

AT-112 Scheduler power button isn't set properly on page load


v1.3.1
------

**New Feature**

AT-108 Add licensing

AT-109 Add master scheduler enable/disable switch

AT-110 Add "show password" checkbox to placevalues

**Improvement**

AT-97 Format the seconds count for progress bars, 3m, 40s instead of 220s

AT-98 PG doesn't create indexes on foreign keys by default. Add these to the ORM model

AT-100 Add "with\_ploymorphic = '\*' to improve performance of loading joined table inheritance.

**Bug**

AT-96 Stopping an execution causes it to display green in the dashboard and the execute list

AT-103 Uploading files to Attune archive causes error message

AT-104 Showing Last 20/20 line only ever shows 20/20 on execute screen

## Task

AT-99 Move auto-list-panel-height directive to RapUI


v1.3.0
------

**Improvement**

AT-23 Resize Plan and Group Step comment fields

AT-71 Convert remainder of Plan and Execute screens to use AngularJS

AT-72 Change Attune Web App from full html page reloads to dynamically loading content as required.

AT-73 Add support for archiving executions to disk

AT-75 On execute screen, highlight failed executions as red

AT-76 On execute screen, add total progress bar

AT-77 Add support for filtering out scheduled executions

AT-78 Add success/failure colours to scheduled task list

AT-79 Display the summation of the time taken by sub steps in the group step on the execute screen

AT-80 Add copyright and separate build number on Attune version

**Story**

AT-63 Create dashboard for running plans

**Bug**

AT-22 Old style handlers, such as the edit step popup, send data to all clients

AT-29 Show/Hide button missing from plan popup

AT-31 Executions need to ignore updates if its older than the last update.

AT-62 Setting keep executions to 0 causes Attune to only run the first procedure

AT-70 Some parts of Attune still refer to the software as SynBUILD

AT-74 Deploy archive does not auto bootstrap linux ssh keys

AT-81 StepWindowsCommandRunner throws exception when it fails to connect

AT-82 151204 Alembic won't upgrade due to python import dependencies

AT-83 Rename the system menu option to upgrade

AT-84 Improve performance of execution data base interaction

AT-85 Buffer and cache the result updates to the UI, including log lines

AT-86 Add build number to Attune build file name
