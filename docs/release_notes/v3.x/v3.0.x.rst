.. _release_notes_v3.0.x:


=============================
Release notes - Attune v3.0.x
=============================

v3.0.29
-------

**Bug**

AT-342 LDAP doesn't work for simple / non role based use cases

v3.0.28
-------

**Bug**

AT-337 Job List and Dashboard Lists cause high Attune server load

v3.0.27
-------

**Bug**

AT-304 WinRM steps fail with "Preparing modules for first use"

v3.0.26
-------

**Bug**

AT-304 WinRM steps fail with "Preparing modules for first use"

AT-307 Fix kerberos dependency issues when deploying Attune offline

v3.0.25
-------

**Bug**

AT-283 TCP Ping steps take too long to timeout, and end up not noticing servers being down.

AT-304 WinRM steps fail with "Preparing modules for first use"

v3.0.22
-------

**Improvement**

AT-306 Add "Download Procedure as Text" support

**Bug**

AT-307 Fix kerberos dependency issues when deploying Attune offline

v3.0.21
-------

**Improvement**

AT-306 Add "Download Procedure as Text" support

**Bug**

AT-259 SECURITY Plan Manual export DOESN'T mask passwords - It needs to do this.

AT-303 Emails from scheduled tasks fail in v3.0.20

AT-304 WinRM steps fail with "Preparing modules for first use"

v3.0.20
-------

**Task**

AT-299 Add support for Windows Kerberos authentication

AT-300 Highlight changes when importing procedures

v3.0.19
-------

**Improvement**

AT-290 Archives: Make Table of Contents list update on Lock/Unlock

v3.0.18
-------

**Improvement**

AT-320 IPv6 Subnet - Add variable for subnet bits, not 255.255.255.192

**Bug**

AT-275 Creating job creates duplicate steps when steps are present twice in procedure

AT-279 Plans some times show duplicate variables

AT-280 Attune doesn't print the last line of log output

AT-281 Attune doesn't handle private key decryption failures.

**Task**

AT-288 Change release build from pip download to wheel

AT-291 Add PowerCLI and Boot ISO steps to Attune install instructions

v3.0.17
-------

**Improvement**

AT-255 Make deploy locked tar files use the OS ssh command instead of piping it through pythons SSH

AT-256 Improve performance of SQL queries to update the UI due to slow HyperV performance

AT-258 Implement support for SSL

## New Feature

AT-249 Implement support for parallel steps

**Bug**

AT-236 Download plan as HTML links don't work

AT-247 "Product of Synerty" should be "Powered By Synerty"

AT-254 Fix compatibility issues for macOS

AT-260 Template config step Generic Server parameters don't show

AT-262 Scheduler no longer stops on a procedure that fails, it carries on

AT-263 Add more ForiegnKey ON\_DELETE constraints

AT-264 UI Job undefined execute value bug fix

AT-265 Schedules fail to generate job log

AT-267 Schedule Repeating

AT-272 Deploying windows archives exceeds attunes file-max ulimit

AT-273 WinRM steps fail with error message XML "Preparing modules for first use."

AT-274 Plans should allow windows/linux server/cred values for the "generic" variables

v3.0.14
-------

**Bug**

AT-243 Server Group value items don't have an order

v3.0.8
------

**Improvement**

AT-238 Add support for LDAP Auth against different AD DS folders

**Bug**

AT-239 Improve error message from database integrity errors

v3.0.2
------

**Bug**

AT-232 Error while importing procedure :  null value in column "osCredId" violates not-null constraint

AT-234 Download results fail when there is a # in the name

**Task**

AT-235 Update DnD upgrade to v3.x.x

v3.0.1
------

**Improvement**

AT-228 Convert Attune update and packaging to use pip and systemctl for init

AT-229 Add log rotater to logging setup

**Bug**

AT-231 Values are not imported with the correct inherited class

v3.0.0
------

**Improvement**

AT-220 Add support for RHEL7 installs

AT-221 Update licensing metrics to schedules and target servers

AT-222 Add support for AD authentication, via LDAP

AT-223 Add support for multiple uers

AT-224 Move built in product docs to read-the-docs

**Story**

AT-230 Exporting plan to HTML silently fails when passwords are not set

**Task**

AT-225 Upgrade UI to angular 6
