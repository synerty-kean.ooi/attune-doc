.. _release_notes_v4.0.x:


=============================
Release notes - Attune v4.0.x
=============================

v4.0.0
------

**Improvement**

AT-268 Upgrade Python to 3.6.8

AT-317 Move Attune LargeFileRequest to txHttpUtil

AT-319 Adapt Attune to use the latest VortexJS/VortexPY from Peek

AT-324 Implement PLPython3 calls

AT-325 Write all job updates to the DB and remove memory caching \(Log, Progress, State\)

AT-326 Change all loads of job status to load from the DB with PLPython3

AT-332 Create new Angular10 jobs page

AT-358 Style Login Screen

**Bug**

AT-253 gitpython 3.x.x doesn't run in Python 2.7

AT-316 Attune 'Deploy Archive' steps use blowfish cipher for large files by default.

AT-363 Unable to export procedures as text or .atp files

**Task**

AT-351 Reformat with Black, Prettier and fix TSLink
